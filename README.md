# Тестовое задание

1. Создать базу данных с названием **db** через phpmyadmin.

Доступы:

URL: [http://localhost:8080/](http://localhost:8080/)

Login: root

Password: pass

2. Накатить миграции 
    
`$ docker-compose exec php php bin/console migrations:migrate`

3. Имопртировать запросы в Postman из файла **vacancy-backend.postman_collection.json** для проверки

4. Запуск PHPUnit

`$ docker-compose exec -T php ./vendor/bin/phpunit --colors=always --configuration ./`