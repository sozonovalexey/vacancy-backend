<?php

/**
 * Load bootstrap file.
 */
require_once __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap.php';

$em = \App\Classes\Helper::em();

$helperSet = new \Symfony\Component\Console\Helper\HelperSet([
    'db'       => new \Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper(\App\Classes\Helper::db()),
    'em'       => new \Doctrine\ORM\Tools\Console\Helper\EntityManagerHelper(\App\Classes\Helper::em()),
    'dialog'   => new \Symfony\Component\Console\Helper\DialogHelper(),
    'question' => new \Symfony\Component\Console\Helper\QuestionHelper(),
]);

$cli = new \Symfony\Component\Console\Application('Console (CodeIgniter integration)', '1.0');
$cli->setHelperSet($helperSet);
$cli->addCommands([
    // Migrations Commands
    new \Doctrine\DBAL\Migrations\Tools\Console\Command\DiffCommand(),
    new \Doctrine\DBAL\Migrations\Tools\Console\Command\ExecuteCommand(),
    new \Doctrine\DBAL\Migrations\Tools\Console\Command\GenerateCommand(),
    new \Doctrine\DBAL\Migrations\Tools\Console\Command\MigrateCommand(),
    new \Doctrine\DBAL\Migrations\Tools\Console\Command\StatusCommand(),
    new \Doctrine\DBAL\Migrations\Tools\Console\Command\VersionCommand(),
]);

$cli->run();