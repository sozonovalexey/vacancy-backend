<?php

namespace Tests\Feature;

use App\Classes\Helper;
use PHPUnit\Framework\TestCase;

class AdsTest extends TestCase
{
    public function testAdsTableExists()
    {
        $schemaManager = Helper::db()->getSchemaManager();
        $result = $schemaManager->tablesExist(['ads']);

        $this->assertEquals(true, $result);
    }
}