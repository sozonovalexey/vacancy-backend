<?php

/**
 * Database connection.
 */

$db = [
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'password' => 'pass',
    'dbname'   => 'db',
    'host'     => 'mysql',
    'charset'  => 'UTF8',
];