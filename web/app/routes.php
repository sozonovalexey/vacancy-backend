<?php

use App\Classes\Router;

Router::post('/ads', 'App\Controllers\AdsController');

Router::post('/ads/([0-9]*)', 'App\Controllers\AdsUpdateController');

Router::get('/ads/relevant', 'App\Controllers\AdsRelevantController');
