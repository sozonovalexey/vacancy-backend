<?php

namespace App\Controllers;

use App\Classes\JsonResponse;
use App\Classes\RedisCache;
use App\Models\AdModel;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdsRelevantController
 *
 * @package App\Controllers
 */
class AdsRelevantController extends BaseController
{
    /**
     * @return void
     */
    public function __invoke(): void
    {
        $tempArr = [];

        $ads = AdModel::repository()->findAll();

        $redisCache = new RedisCache();
        $redis = $redisCache->getRedis();

        foreach ($ads as $ad) {
            $currentLimit = $redis->get($ad->getId());
            if ($currentLimit >= $ad->getLimit()) {
                continue;
            }

            for ($i = 0; $i < $ad->getLimit(); ++$i) {
                $tempArr[] = $ad;
            }
        }

        if (!$tempArr) {
            JsonResponse::create([
                'message' => Response::$statusTexts[Response::HTTP_NO_CONTENT],
                'code'    => Response::HTTP_NO_CONTENT,
                'data'    => [],
            ], Response::HTTP_OK)->send();
        }

        $key = array_rand($tempArr);
        $ad = $tempArr[$key];

        if ($prevValue = $redis->get($ad->getId())) {
            $newValue = $prevValue + 1;
        }
        else {
            $newValue = 1;
        }

        $redis->set($ad->getId(), $newValue);

        JsonResponse::create([
            'message' => Response::$statusTexts[Response::HTTP_OK],
            'code'    => Response::HTTP_OK,
            'data'    => $ad->toArray(),
        ])->send();
    }
}