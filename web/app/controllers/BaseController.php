<?php

namespace App\Controllers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\RecursiveValidator;

/**
 * Class BaseController
 *
 * @package App\Controllers
 */
class BaseController
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * BaseController constructor.
     */
    public function __construct()
    {
        $this->request = Request::createFromGlobals();
    }

    /**
     * @return RecursiveValidator
     */
    protected function getValidator(): RecursiveValidator
    {
        $validator = Validation::createValidatorBuilder();
        $validator->addMethodMapping('loadValidatorMetadata');
        return $validator->getValidator();
    }
}