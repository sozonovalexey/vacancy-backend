<?php

namespace App\Controllers;

use App\Classes\JsonResponse;
use App\Models\AdModel;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AdsUpdateController
 *
 * @package App\Controllers
 */
class AdsUpdateController extends BaseController
{
    /**
     * @param int $id AdModel model id.
     *
     * @return void
     */
    public function __invoke(int $id): void
    {
        /** @var AdModel $ad */
        if (!$ad = AdModel::repository()->find($id)) {
            JsonResponse::create([
                'message' => 'Not found!',
                'code'    => Response::HTTP_BAD_REQUEST,
                'data'    => [],
            ])->send();
        }

        if ($banner = $this->request->get('banner')) {
            $ad->setBanner($banner);
        }

        if ($limit = $this->request->get('limit')) {
            $ad->setLimit($limit);
        }

        if ($price = $this->request->get('price')) {
            $ad->setPrice($price);
        }

        if ($text = $this->request->get('text')) {
            $ad->setText($text);
        }

        $ad->save(true);

        JsonResponse::create([
            'message' => Response::$statusTexts[Response::HTTP_OK],
            'code'    => Response::HTTP_OK,
            'data'    => $ad->toArray(),
        ])->send();
    }
}