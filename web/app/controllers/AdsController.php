<?php

namespace App\Controllers;

use App\Classes\JsonResponse;
use App\Models\AdModel;
use App\Validators\AdValidator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * Class AdsController
 *
 * @package App\Controllers
 */
class AdsController extends BaseController
{
    /**
     * @return void
     */
    public function __invoke(): void
    {
        $requestContent = $this->request->getContent();
        if (!is_array($requestContent)) {
            parse_str($requestContent, $params);
            $requestContent = $params;
        }

        $validator = $this->getValidator();
        $input = new AdValidator($requestContent);
        $violations = $validator->validate($input);
        if ($violations->count()) {
            /** @var ConstraintViolation $error */
            $error = $violations->get(0);
            JsonResponse::create([
                'message' => $error->getMessage(),
                'code'    => Response::HTTP_BAD_REQUEST,
                'data'    => [],
            ], Response::HTTP_BAD_REQUEST)->send();
        }

        $ad = AdModel::create()
            ->setText($input->getText())
            ->setPrice($input->getPrice())
            ->setLimit($input->getLimit())
            ->setBanner($input->getBanner())
            ->save(true);

        JsonResponse::create([
            'message' => Response::$statusTexts[Response::HTTP_OK],
            'code'    => Response::HTTP_OK,
            'data'    => $ad->toArray(),
        ])->send();
    }
}