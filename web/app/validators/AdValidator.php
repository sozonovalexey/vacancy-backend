<?php

namespace App\Validators;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * Class AdValidator
 *
 * @package App\Validators
 */
final class AdValidator
{
    /**
     * @var string Ссылка на картинку.
     */
    private $banner;

    /**
     * @var int Лимит показов.
     */
    private $limit;

    /**
     * @var int Стоимость одного показа.
     */
    private $price;

    /**
     * @var string Заголовок объявления.
     */
    private $text;

    /**
     * AdValidator constructor.
     *
     * @param array|ParameterBag $params
     */
    public function __construct($params)
    {
        if (is_array($params)) {
            $params = new ParameterBag($params);
        }

        $this->banner = $params->get('banner');
        $this->limit = (int)$params->get('limit');
        $this->price = (int)$params->get('price');
        $this->text = $params->get('text');
    }

    /**
     * This method is where you define your validation rules.
     *
     * @param ClassMetadata $metadata
     *
     * @return void
     */
    public static function loadValidatorMetadata(ClassMetadata $metadata): void
    {
        $metadata
            ->addPropertyConstraints('banner', [
                new Assert\NotBlank([
                    'message' => 'Invalid banner link'
                ]),
            ])
            ->addPropertyConstraints('limit', [
                new Assert\NotBlank([
                    'message' => 'Invalid limit'
                ]),
                new Assert\Type([
                    'type' => 'integer',
                    'message' => 'Invalid limit'
                ]),
                new Assert\NotEqualTo([
                    'value' => 0,
                    'message' => 'Invalid limit'
                ]),
            ])
            ->addPropertyConstraints('price', [
                new Assert\NotBlank([
                    'message' => 'Invalid price'
                ]),
                new Assert\Type([
                    'type' => 'integer',
                    'message' => 'Invalid price'
                ]),
                new Assert\NotEqualTo([
                    'value' => 0,
                    'message' => 'Invalid price'
                ]),
            ])
            ->addPropertyConstraints('text', [
                new Assert\NotBlank([
                    'message' => 'Invalid text'
                ]),
                new Assert\Type([
                    'type' => 'string',
                    'message' => 'Invalid text'
                ]),
            ]);
    }

    /**
     * @return string
     */
    public function getBanner(): string
    {
        return $this->banner;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }
}