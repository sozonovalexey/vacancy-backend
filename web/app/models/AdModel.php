<?php

namespace App\Models;

use App\Classes\Helper;
use App\Repositories\AdRepository;

/**
 * Class AdModel
 *
 * @method static AdRepository repository()
 *
 * @package App\Models
 */
class AdModel extends BaseModel
{
    const PRICE_FIELD = 'price';
    const LIMIT_FIELD = 'limit';

    /**
     * An array of excluded fields.
     *
     * @var string[]
     */
    public static $excludedFields = [
        self::PRICE_FIELD,
        self::LIMIT_FIELD,
    ];

    /**
     * @var int ID.
     */
    private $id;

    /**
     * @var string Ссылка на картинку.
     */
    private $banner;

    /**
     * @var int Лимит показов.
     */
    private $limit;

    /**
     * @var int Стоимость одного показа.
     */
    private $price;

    /**
     * @var string Заголовок объявления.
     */
    private $text;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return AdModel
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getBanner(): string
    {
        return $this->banner;
    }

    /**
     * @param string $banner
     *
     * @return AdModel
     */
    public function setBanner(string $banner): self
    {
        $this->banner = $banner;

        return $this;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     *
     * @return AdModel
     */
    public function setLimit(int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     *
     * @return AdModel
     */
    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     *
     * @return AdModel
     */
    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $metaData = Helper::em()->getClassMetadata(get_called_class());
        $result = [];
        foreach ($metaData->getFieldNames() as $fieldName) {
            if (in_array($fieldName, self::$excludedFields)) {
                continue;
            }
            $result[$fieldName] = $metaData->getFieldValue($this, $fieldName);
        }
        foreach ($metaData->getAssociationMappings() as $associationMapping) {
            $fieldName = $associationMapping['fieldName'];
            if (in_array($fieldName, self::$excludedFields)) {
                continue;
            }
            $result[$fieldName] = $metaData->getFieldValue($this, $fieldName);
        }

        return $result;
    }
}