<?php

namespace App\Models;

use App\Classes\Helper;
use Doctrine\ORM\EntityRepository;

/**
 * Model base class.
 */
class BaseModel
{
    /**
     * @return static
     */
    public static function create(): BaseModel
    {
        return new static();
    }

    /**
     * @param bool $flush
     *
     * @return $this
     */
    public function save($flush = false): BaseModel
    {
        try {
            $em = Helper::em();
            $em->persist($this);
            !$flush or $em->flush($this);
        }
        catch (\Exception $exception) {
            //
        }

        return $this;
    }

    /**
     * @return EntityRepository
     */
    public static function repository(): EntityRepository
    {
        return Helper::em()->getRepository(get_called_class());
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $metaData = Helper::em()->getClassMetadata(get_called_class());
        $result = [];
        foreach ($metaData->getFieldNames() as $fieldName) {
            $result[$fieldName] = $metaData->getFieldValue($this, $fieldName);
        }
        foreach ($metaData->getAssociationMappings() as $associationMapping) {
            $fieldName = $associationMapping['fieldName'];
            $result[$fieldName] = $metaData->getFieldValue($this, $fieldName);
        }

        return $result;
    }
}