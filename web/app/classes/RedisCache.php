<?php

namespace App\Classes;

/**
 * Class RedisCache
 *
 * @package App\Classes
 */
class RedisCache extends \Doctrine\Common\Cache\RedisCache
{
    const HOST = 'redis';
    const PORT = 6379;

    /**
     * @param array $args
     *
     * @throws \Exception
     */
    function __construct(array $args = [])
    {
        $dbIndex = $args['dbIndex'] ?? 0;
        $connect = array_merge(
            [
                'host' => self::HOST,
                'port' => self::PORT,
            ],
            (isset($args['connect']) && is_array($args['connect']) ? $args['connect'] : [])
        );
        $className = '\Redis';
        if (!class_exists($className)) {
            throw new \Exception('Class ' . $className . ' does not exist', null);
        }
        $redis = new \Redis;
        $redis->connect($connect['host'], $connect['port']);
        $redis->select($dbIndex);
        $this->setRedis($redis);
    }

    /**
     * @param null|int $dbIndex
     * @return bool
     */
    public function select($dbIndex = null)
    {
        if (null !== $dbIndex && $this->getRedis()) {
            return $this->getRedis()->select($dbIndex);
        }

        return false;
    }

    /**
     * @return \Redis|null
     */
    public function getRedis(): ?\Redis
    {
        return parent::getRedis();
    }
}
