<?php

namespace App\Classes;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\YamlDriver;
use Doctrine\ORM\ORMException;
use Gedmo\DoctrineExtensions;

/**
 * Class DoctrineLibrary
 *
 * @package App\Classes
 */
class DoctrineLibrary
{
    // Path of app
    const APP_PATH = __DIR__ . DIRECTORY_SEPARATOR . '..';

    // Path of application root
    const BASE_PATH = self::APP_PATH . DIRECTORY_SEPARATOR . '..';

    /**
     * @var EntityManager
     */
    protected static $instance;

    /**
     * @var EntityManager|null
     */
    public $em;

    /**
     * DoctrineLibrary constructor.
     *
     * @throws ORMException
     */
    public function __construct()
    {
        // set up the configuration
        $config = new Configuration();

        // set up proxy configuration
        $config->setProxyDir(self::APP_PATH . DIRECTORY_SEPARATOR . 'proxies');
        $config->setProxyNamespace('App\Proxies');
        $config->setAutoGenerateProxyClasses(true);

        // metadata cache provider
        $cacheProvider = new ArrayCache();
        $config->setMetadataCacheImpl($cacheProvider);

        $file = [
            self::BASE_PATH,
            'vendor',
            'doctrine',
            'orm',
            'lib',
            'Doctrine',
            'ORM',
            'Mapping',
            'Driver',
            'DoctrineAnnotations.php',
        ];
        $file = implode(DIRECTORY_SEPARATOR, $file);
        // ensure standard doctrine annotations are registered
        AnnotationRegistry::registerFile($file);

        // standard annotation reader
        $annotationReader = new AnnotationReader();
        $cachedAnnotationReader = new CachedReader(
            $annotationReader, // use reader
            $cacheProvider // and a cache driver
        );
        // create a driver chain for metadata reading
        $driverChain = new MappingDriverChain();
        // load superclass metadata mapping only, into driver chain
        // also registers Gedmo annotations.NOTE: you can personalize it
        DoctrineExtensions::registerAbstractMappingIntoDriverChainORM(
            $driverChain, // our metadata driver chain, to hook into
            $cachedAnnotationReader // our cached annotation reader
        );

        // set up app entities driver
        $driverYml = new YamlDriver(self::APP_PATH . DIRECTORY_SEPARATOR . 'mappings');
        $driverChain->addDriver($driverYml, 'App\Models');
        $config->setMetadataDriverImpl($driverChain);

        // Tells Doctrine what mode we want
        $isDevMode = true;

        require self::APP_PATH . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'database.php';

        $dbConfig = &$db;

        // Doctrine connection configuration
        $dbParams = [
            'driver'   => $dbConfig['driver'],
            'user'     => $dbConfig['user'],
            'password' => $dbConfig['password'],
            'dbname'   => $dbConfig['dbname'],
            'host'     => $dbConfig['host'],
            'charset'  => $dbConfig['charset'],
        ];

        static::$instance = EntityManager::create($dbParams, $config);
        $this->em = &static::$instance;
    }

    /**
     * @return EntityManager
     */
    public static function getInstance(): EntityManager
    {
        if (!isset(static::$instance) || !static::$instance->isOpen()) {
            new DoctrineLibrary();
        }

        return self::$instance;
    }


    /**
     * @return EntityManager
     */
    public static function em()
    {
        return static::getInstance();
    }

    /**
     * @return Connection
     */
    public static function db(): Connection
    {
        return static::em()->getConnection();
    }
}