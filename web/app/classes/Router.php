<?php

namespace App\Classes;

use JetBrains\PhpStorm\NoReturn;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Router
 *
 * @package App\Classes
 */
class Router
{
    /**
     * @var array An array of routes.
     */
    private static $routes = [];

    /**
     * @param string $expression
     * @param string $controllerName
     */
    public static function get(string $expression, string $controllerName): void
    {
        self::add($expression, $controllerName, Request::METHOD_GET);
    }

    /**
     * @param string $expression
     * @param string $controllerName
     */
    public static function post(string $expression, string $controllerName): void
    {
        self::add($expression, $controllerName, Request::METHOD_POST);
    }

    /**
     * @param string $expression
     * @param string $controllerName
     * @param string $method
     */
    public static function add(string $expression, string $controllerName, string $method = Request::METHOD_GET): void
    {
        array_push(self::$routes, [
            'expression'     => $expression,
            'controllerName' => $controllerName,
            'method'         => $method
        ]);
    }

    public static function run(): void
    {
        $request = Request::createFromGlobals();
        $requestUri = $request->getRequestUri();
        $requestMethod = $request->getMethod();

        foreach(self::$routes as $route) {

            /**
             * @var string|null $expression
             * @var string|null $controllerName
             * @var string|null $method
             */
            extract(array_merge([
                'expression'     => '',
                'controllerName' => null,
                'method'         => Request::METHOD_GET,
            ], $route));

            $expression = '^' . $expression . '$';
            if (preg_match('#' . $expression . '#', $requestUri, $matches) && $method === $requestMethod) {
                array_shift($matches);
                call_user_func_array(new $route['controllerName'](), $matches);
                exit();
            }
        }

        header('HTTP/1.0 404 Not Found');
        exit();
    }
}