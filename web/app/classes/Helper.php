<?php

namespace App\Classes;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;

/**
 * Class Helper
 *
 * @package App\Classes
 */
class Helper
{
    /**
     * @return EntityManager
     */
    public static function em(): EntityManager
    {
        return DoctrineLibrary::em();
    }

    /**
     * @return Connection
     */
    public static function db(): Connection
    {
        return DoctrineLibrary::db();
    }
}