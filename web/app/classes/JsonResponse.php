<?php namespace App\Classes;

use Symfony\Component\HttpFoundation\JsonResponse as BaseJsonResponse;

/**
 * Class JsonResponse
 *
 * @package App\Classes
 */
class JsonResponse extends BaseJsonResponse
{
    /**
     * @param mixed $data
     * @param int   $status
     * @param array $headers
     *
     * @return JsonResponse
     */
    public static function create($data = null, $status = 200, $headers = []): JsonResponse
    {
        null !== $status or $status = BaseJsonResponse::HTTP_OK;
        $result = parent::create(is_string($data) ? null : $data, $status, $headers);
        if (is_string($data)) {
            $result->setContent($data);
        }

        return $result;
    }
}
